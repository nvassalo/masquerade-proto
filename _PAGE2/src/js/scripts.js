var TJ = THREE;
var APP = APP || {};
var scene; // THREEJS

var vShader = `
	uniform float intensity;
	uniform float time;
	void main() {

		vec3 p = position;

		p.x = position.x  + (sin(intensity) * 50.0) + (sin(time) + 10.0);
		p.y = position.y  - (cos(intensity) * 20.0) + (cos(time) + 20.0);

		vec4 modelViewPosition = modelViewMatrix * vec4(p.xy, position.z, 1.0);
		gl_Position = projectionMatrix * modelViewPosition;
	}
`;

var fShader = `
	uniform float intensity;
	void main() {
		float a = intensity;
		gl_FragColor = vec4(1.0, a, 1.0, 1.0);
	}
`	
var Store = Store || {
	lyrics: {
		paragraphs: [
			{
				lines: [
					{
						words: [
							{ 
								word: "Many",
								timing: 0.2,
								effect: "swirl"
							},
							{
								word: "moons",
								timing: 0.4,
								effect: null
							},
							{
								word: "yonder"
							}
						]
					},
					{
						words: [
							{
								word: "On",
								timing: 0,
								effect: null
							},
							{
								word: "an",
								timing: 0,
								effect: null
							},
							{
								word: "eve",
								timing: 0,
								effect: null
							},
							{
								word: "just",

							},
							{
								word: "like"
							},
							{
								word: "the",
							},
							{
								word: "last."
							}
						]
					},
					{
						words: [
							{
								word: "The"
							},
							{
								word: 	"vines"
							},
							{
								word: "did"	
							},
							{
								word: "whisper"	
							}
						]
					},
					{
						words: [
							{
								word: 	"Of"
							},
							{
								word: 	"an"
							},
							{
								word: 	"astral"
							},
							{
								word: 	"ball,"
							},
							{
								word: 	"(astral"
							},
							{
								word: 	"ball)"
							}
						]
					},
					{
						words: [
							{
								word: "And"
							},
							{
								word: "the"
							},
							{
								word: "tale"
							},{
								word: "/"
							},
							{
								word: "of"
							},
							{
								word: "two"
							},
							{
								word: "masks."
							}
						]
					}

				]
			},
			{
				lines: [
					{
						words: [
							{
								word: "In"
							},
							{
								word: "a"
							},
							{
								word: "place"
							},
							{
								word: "betwixt"
							},
							{
								word: "time"
							},
							{
								word: "(betwixt"
							},
							{
								word: "time)"
							}
						]
					},
					{
						words: [
							{							
								word: "Exempt"
							},
							{							
								word: "of"
							},
							{							
								word: "day."
							}
						]
					},
					{
						words: [
							{
								word: "Where"
							},
							{
								word: "the"
							},
							{
								word: "future"
							},
							{
								word: "doth"
							},
							{
								word: "twirl"
							}
						]
					},
					{
						words: [
							{
								word: "With"
							},
							{
								word: "the",
							},
							{
								word: "past"
							},
							{
								word: "who'll"
							},
							{
								word: "sway."
							}
						]
					}
				]
			},
			{
				lines: [
					{
						words: [
							{
								word: "Dame"
							},
							{
								word: "destiny"
							},
							{
								word: "saunters,"
							}
						]
					},
					{
						words: [
							{
								word: "Through"
							},
							{
								word: "presence/"
							},
							{
								word: "she'll"
							},
							{
								word: "fleet."
							}
						]
					},
					{
						words: [
							{
								word: "In"
							},
							{
								word: "a"
							},
							{
								word: "chance"
							},
							{
								word: "suspension,"
							}
						]
					},
					{
						words: [
							{
								word: "Across"
							},
							{
								word: "dimension"
							}
						]
					},
					{
						words: [
							{
								word: "Thou"
							},
							{
								word: "could"
							},
							{
								word: "meet."
							},
						]
					}
				]
			},
			{
				lines:[
					{
						words: [
							{
								word: "Yet"
							},
							{
								word: "crossings"
							},
							{
								word: "are"
							},
							{
								word: "transient"
							}
						]
					},
					{
						words: [
							{
								word: "In"
							},
							{
								word: "the"
							},
							{
								word: "perpetual"
							},
							{
								word: "ballroom"
							},
							{
								word: "commotion."
							}
						]
					},
					{
						words: [
							{
								word: "A"
							},
							{
								word: "web"
							},
							{
								word: "so"
							},
							{
								word: "tightly"
							},
							{
								word: "weft",
							},
							{
								word: "and",
							},
							{
								word: "woven,"
							},
						]
					},
					{
						words: [
							{
								word: "Scarce"
							},
							{
								word: "are"
							},
							{
								word: "the"
							},
							{
								word: "odds"
							}
						]
					},
					{
						words: [
							{
								word: "Of"
							},
							{
								word: "such"
							},
							{
								word: "a"
							},
							{
								word: "divine"
							},
							{
								word: "collision."
							}
						]
					}
				]
			},
			{
				lines:[
					{
						words: [
							{
								word: "She"
							},
							{
								word: "a Queen"
							},
							{
								word: "of"
							},
							{
								word: "cobalt"
							},
							{
								word: "moon,"
							}
						]
					},			

					{
						words: [
							{
								word: "He"
							},
							{
								word: "a"
							},
							{
								word: "knight"
							},
							{
								word: "of"
							},
							{
								word: "shadow"
							},
							{
								word: "in"
							},
							{
								word: "dusk."
							}

						]
					},
				
					{
						words: [
							{
								word: "Two"
							},
							{
								word: "disparate"
							},
							{
								word: "souls"
							},
							{
								word: "in"
							},
							{
								word: "demi-disguise"
							}
						]
					},
					{
						words: [
							{
								word: "Feel"
							},
							{
								word: "a"
							},
							{
								word: "familiar"
							},
							{
								word: "breeze"
							},
							{
								word: "curl"
							},
							{
								word: "the"
							},
							{
								word: "spine"
							}
						]
					},
					{
						words: [
							{
								word: "As"
							},
							{
								word: "the"
							},
							{
								word: "stars"
							},
							{
								word: "align"
							}
						]
					},
					{
						words: [
							{
								word: "Through"
							},
							{
								word: "their"
							},
							{
								word: "mirrored"
							},
							{
								word: "Masque…"
							}
						]
					}
				]
			}
		]
	}
}

function map (num, in_min, in_max, out_min, out_max) {
  return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


APP.main = (function() {


	
	                                              
	        /*lines.map( line => line.map(
	             words.map( word => {
	                 console.log(word)                 		
	             }
	)))))*/
	scene =  new TJ.Scene();
	var wW = window.innerWidth;
	var wH = window.innerHeight;
	var frustumSize = window.innerHeight;
	var aspect = window.innerWidth / window.innerHeight;
	var camera = new THREE.OrthographicCamera( frustumSize * aspect / - 2, frustumSize * aspect / 2, frustumSize / 2, frustumSize / - 2, -20, 2000 );
	// camera.position.set(0, 0, 0);
	var canvas = document.getElementById('scene');
	var renderer = new TJ.WebGLRenderer({antialias: true});
	renderer.setSize( window.innerWidth, window.innerHeight ); 
	renderer.setClearColor( 0x000000, 1 );
	var lyrics = Store.lyrics;
	var mesh, material, geometry;
	var audioLoader = new TJ.AudioLoader();
	var listener = new TJ.AudioListener();
	var audio = new TJ.Audio( listener );
	var fx_listener = new TJ.AudioListener();
	var fx = new TJ.Audio(fx_listener);
	var loader = new TJ.FontLoader();
	var block = new TJ.Group();
	var analyser = new THREE.AudioAnalyser( audio, 32 );
	var uniforms = {
		intensity: {
			value: 1.0,
		},
		time: { value: 0.0 }
	}

	var initThree = function() {
		

		$('body').prepend('<h1 class="loading" style="margin:0;width: 100vw; height: 100vh; background: #000;color: #fff;position: fixed; top: 0; left: 0">Loading</h1>');
		loader.load( 'fonts/helvetiker_bold.typeface.json', function(font){
			var xMid;
			var yMid;
			

			var posY = 0;
			var fontSize = 50;
			var lineHeight = 70;
			_material = new TJ.ShaderMaterial({
				uniforms: uniforms,	
				vertexShader: vShader,
				fragmentShader: fShader
			});
			var lineDiff = 0;


			lyrics.paragraphs.map( function(paragraph, k){
				var paragraphGroup = new TJ.Group();
				paragraphGroup.name = "paragraph_" + (k + 1);
				paragraph.lines
				.map(function(line, i){
						var lineGroup = new TJ.Group();

						lineDiff += lineHeight;
						var wordX = 0;
						var lineWidth = 0;
						lineGroup.userData.time = line.words[0].timing;
						console.log("lineG", lineGroup.userData);

						line.words.map( function(word, j) {
							word.word = j > 0 ? " " + word.word : word.word;
							var _textShape = new TJ.BufferGeometry();
							var shapes = font.generateShapes( word.word, fontSize, 12 );
							var _geometry = new TJ.ShapeGeometry( shapes );
							_textShape.fromGeometry( _geometry );
							_textShape.translate(wordX, 0 , 0);
							_textShape.computeBoundingBox();
							wordX += ( _textShape.boundingBox.max.x - _textShape.boundingBox.min.x );

							var _mesh = new TJ.Mesh(_textShape, _material);
							_mesh.userData.time = line.words[j].timing;

							//var box = new TJ.BoxHelper(_mesh, 0xff0000);
							lineGroup.add(_mesh);
							lineGroup.name = "line_" + (j + 1);
							lineGroup.position.set(0 - wordX / 2, 0 - lineDiff, -1);

						}
						
					);
						paragraphGroup.add(lineGroup);
						paragraphGroup.translateY(-lineHeight*0.2);
						block.name = "paragraph" + (i + 1);

				}
			)
				block.add(paragraphGroup);
				block.name = "main";
			})

			audioLoader.load('/assets/audio/prologue.mp3', function(buffer) {
				audio.setBuffer( buffer );
				console.log(audio);
				audioLoader.load('/assets/audio/fx.mp3', function(buffer2) {
					fx.setBuffer(buffer2)
					//audio.play();
					//fx.play();
					scene.add( block );

					//block.position.set(0, wH , -1);
					
					// make shape ( N.B. edge view not visible )
					canvas.appendChild( renderer.domElement );
					$('.loading').fadeOut(1000, function() {
						$(this).remove();
					});
					window.scene = scene;
					console.log(block);
					render();
					window.addEventListener('touchstart', audio.play());
					document.addEventListener('click', audio.play());
				})	
			})


			

			$(window).on('resize', resizeUpdate());


		});
	};

	
	var y = 1;

	var resizeUpdate = function() {
		wW = window.innerWidth;
		vH = window.innerHeight;
	}

	var update = function () {
		if ( y >= 0) {
			y+=(Math.pow(y,-0.1));

		} else if ( y > wH ) {
			y-=0.05;
		}
		block.position.y = y;
		var average = 0;
		var arr = analyser.getFrequencyData().slice(4).slice(-8).map(  function(number) {
			average += number;
			return number;
		});
		average = average / arr.length;
		console.log(average, arr);
		_material.uniforms['intensity'].value = map(average, 0, 60, 0.2, 1.0);
		_material.uniforms['time'].value++;

	}

	var render = function () {


		update();
		renderer.render( scene, camera );
		
		window.requestAnimationFrame( render );


	};
	var init = function() {
		initThree() 
	}

	return {
		init: init
	}
		
})();



$(document).ready(APP.main.init)