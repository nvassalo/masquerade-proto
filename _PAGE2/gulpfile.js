var gulp = require('gulp');
var babel = require('gulp-babel');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require("browserify");
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var nano = require('cssnano');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var gutil = require('gulp-util');

var JS_SOURCE = 'src/js';
var JS_DEST = 'public/js';
var JS_OUTPUT_FILE = 'main.js';
var CSS_SOURCE = 'src/scss';
var CSS_DEST = 'public/css';
var SERVER_BASE_DIR = 'public';
var WATCH_FILE_EXTENSIONS = ['*.scss', '.js'];

var processors = [
  autoprefixer({browsers: ['last 2 versions', 'ie >= 11']}),
  nano()
];

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: SERVER_BASE_DIR
    }
  });
});

gulp.task('bs-reload', function() {
  browserSync.reload({stream: true});
});

gulp.task('scripts', function() {
  return browserify('./src/js/scripts.js', {debug:true})
    .transform('babelify')
    .bundle()     
     .pipe(plumber({
       errorHandler: function(error) {
         notify.onError({
               title: "Gulp error in " + error.plugin,
               message:  error.toString()
           })(error);
           gutil.beep(3);
     }}))   
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(gulp.dest('public/js'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('styles', function() {
   return gulp.src(CSS_SOURCE + '/base.scss')
    .pipe(plumber({
      errorHandler: function(error) {
          notify.onError({
              title: "Gulp error in " + error.plugin,
              message:  error.toString()
          })(error);
          // play a sound once
          gutil.beep(2);
    }}))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'nested'}))
    .pipe(concat('main.css'))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(CSS_DEST + '/'))
    .pipe(browserSync.reload({stream:true}))
});



gulp.task('dist-scripts', function() {
  return gulp.src('./src/js/scripts.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
})

gulp.task('default', ['browser-sync'], function() {
  gulp.run('scripts', 'styles');
  gulp.watch([JS_SOURCE + '/**/*.js', '!./js/dest/*.js'], ['scripts']);
  gulp.watch(CSS_SOURCE + '/**/*.scss', ['styles']);
  gulp.watch(WATCH_FILE_EXTENSIONS, ['bs-reload']);
});



